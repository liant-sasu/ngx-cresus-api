# NgxCresusApi

This library allow to communicate with cresus API backend's services.

You can use it freely and make your own client or service around our backend.

Nevertheless you'll need to register your application on our service by contacting
us at contact@liant.dev to be able to access to private sections.

It is hosted on [gitlab](https://gitlab.com/liant-sasu/ngx-cresus-api).

## Build

Run `ng build ngx-cresus-api` to build the project. The build artifacts will be stored in the `dist/` directory.

## Publishing

After building your library with `ng build ngx-cresus-api`, go to the dist folder `cd dist/ngx-cresus-api` and run `npm publish`.

## Running unit tests

Run `ng test ngx-cresus-api` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Issues

Feel free to register any bug on our [Gitlab Page](https://gitlab.com/liant-sasu/ngx-cresus-api/-/issues).