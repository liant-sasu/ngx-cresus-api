# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.0.6] - 2023-04-14

### Added

- MIT License
- Project's product management
- Project's service management

### Fixed

- Bad URL generation on both Charges and Budgets lists
- Date in ITimeable is now a string since they are not parsed

## [0.0.5] - 2023-04-13

Revision made previous to changelog