import { TestBed, fakeAsync, waitForAsync } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing'

import { NgxCresusApiService } from './ngx-cresus-api.service';
import { EFrequencies, IBudget, ICharge, IUser, IVersion } from './models';
import { CommunicateService } from './communicate.service';
import { Observable } from 'rxjs';
import { MeService } from './v0/me.service';
import { BudgetsService } from './v0/budgets.service';
import { ChargesService } from './v0/charges.service';
import { ProjectsService } from './v0/projects.service';
import { UnitsService } from './v0/units.service';
import { ProductsService } from './v0/products.service';
import { ServicesService } from './v0/services.service';

describe('NgxCresusApiService', () => {
  let service: NgxCresusApiService,
      httpTestingController: HttpTestingController,
      comService: CommunicateService,
      budgetsServiceSpy: jasmine.SpyObj<BudgetsService>,
      chargesServiceSpy: jasmine.SpyObj<ChargesService>,
      meServiceSpy: jasmine.SpyObj<MeService>,
      projectsServiceSpy: jasmine.SpyObj<ProjectsService>,
      unitsServiceSpy: jasmine.SpyObj<UnitsService>,
      productsServiceSpy: jasmine.SpyObj<ProductsService>,
      servicesServiceSpy: jasmine.SpyObj<ServicesService>;


  beforeEach(waitForAsync(() => {
    budgetsServiceSpy = jasmine.createSpyObj('BudgetsService', ['list', 'show', 'update', 'create', 'delete']);
    chargesServiceSpy = jasmine.createSpyObj('ChargesService', ['list', 'show', 'update', 'create', 'delete']);
    meServiceSpy = jasmine.createSpyObj('MeService', ['show']);
    projectsServiceSpy = jasmine.createSpyObj('ProjectsService', ['list', 'show']);
    unitsServiceSpy = jasmine.createSpyObj('UnitsService', ['list', 'show', 'update', 'create', 'delete']);
    productsServiceSpy = jasmine.createSpyObj('ProductsService', ['list', 'show', 'update', 'create', 'delete']);
    servicesServiceSpy = jasmine.createSpyObj('ServicesService', ['list', 'show', 'update', 'create', 'delete']);
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        { provide: BudgetsService, useValue: budgetsServiceSpy },
        { provide: ChargesService, useValue: chargesServiceSpy },
        { provide: MeService, useValue: meServiceSpy },
        { provide: ProjectsService, useValue: projectsServiceSpy },
        { provide: UnitsService, useValue: unitsServiceSpy },
        { provide: ProductsService, useValue: productsServiceSpy },
        { provide: ServicesService, useValue: servicesServiceSpy }
      ]
    })
           .compileComponents();
    service = TestBed.inject(NgxCresusApiService);
  }));
  beforeEach(function() {
    httpTestingController = TestBed.inject(HttpTestingController);
    comService = TestBed.inject(CommunicateService);
  });
  
  afterEach(() => {
      httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#update_api_url', () => {
    it('should update the base url used', fakeAsync(() => {
      service.version().subscribe(
        function(data: IVersion) {
          expect(data.version).toEqual('0.0.5')
        }
      );

      const req = httpTestingController.expectOne('https://cresus-api.liant.dev/version');

      req.flush({
          version: '0.0.5'
      });

      service.update_api_url('https://localhost:3000');
      service.version().subscribe(
        function(data: IVersion) {
          expect(data.version).toEqual('0.0.5')
        }
      );

      const req2 = httpTestingController.expectOne('https://localhost:3000/version');

      req2.flush({
          version: '0.0.5'
      });
    }));
  });

  describe('#libVersion', () => {
    it('should return the library version', () => {
      expect(service.libVersion()).toMatch(/0\.0\.\d+/);
    });
  });

  describe('#libVersion', () => {
    it('should get version from the server', fakeAsync(() => {
      service.version().subscribe(
        function(data: IVersion) {
          expect(data.version).toEqual('0.0.5')
        }
      );

      const req = httpTestingController.expectOne(comService.build_url('version', null));

      req.flush({
          version: '0.0.5'
      });
    }));
  });

  describe('#me', () => {
    it('should call MeService#show()', fakeAsync(() => {
      // create `show` spy on an object representing the MeService
      meServiceSpy.show.and.returnValue(
        new Observable(
          (subscriber) => {
            subscriber.next({ id: 'test', email: 'contact@liant.dev' } as IUser)
          }
        )
      );

      service.me();

      expect(meServiceSpy.show).toHaveBeenCalledTimes(1)
    }));
  });

  /* Budgets management tests */

  describe('#budgets', () => {
    it('should call BudgetsService#list()', fakeAsync(() => {
      budgetsServiceSpy.list.withArgs('project_id').and.returnValue(
        new Observable(
          (subscriber) => {
            subscriber.next([
              {
                id: 'test', name: 'Budget', description: 'description',
                project_id: 'id', url: 'une URL',
                created_at: "2023-04-14T05:34:47.782Z", updated_at: "2023-04-14T05:34:47.782Z"
              }
            ] as IBudget[])
          }
        )
      );

      service.budgets('project_id');

      expect(budgetsServiceSpy.list).toHaveBeenCalledTimes(1)
    }));
  });

  describe('#budget', () => {
    it('should call BudgetsService#show()', fakeAsync(() => {
      budgetsServiceSpy.show.withArgs('project_id', 'id').and.returnValue(
        new Observable(
          (subscriber) => {
            subscriber.next({
              id: 'test', name: 'Budget', description: 'description',
              project_id: 'id', url: 'une URL',
              created_at: "2023-04-14T05:34:47.782Z", updated_at: "2023-04-14T05:34:47.782Z"
            } as IBudget);
          }
        )
      );

      service.budget('project_id', 'id');

      expect(budgetsServiceSpy.show).toHaveBeenCalledTimes(1);
    }));
  });

  describe('#create_budget', () => {
    it('should call BudgetsService#create()', fakeAsync(() => {
      budgetsServiceSpy.create.withArgs('project_id', { name: 'Mon Budget', description: 'une description' })
                       .and.returnValue(
        new Observable(
          (subscriber) => {
            subscriber.next({
              id: 'test', name: 'Mon Budget', description: 'une description',
              project_id: 'id', url: 'une URL',
              created_at: "2023-04-14T05:34:47.782Z", updated_at: "2023-04-14T05:34:47.782Z"
            } as IBudget);
          }
        )
      );

      service.create_budget('project_id', { name: 'Mon Budget', description: 'une description' });

      expect(budgetsServiceSpy.create).toHaveBeenCalledTimes(1);
    }));
  });

  describe('#update_budget', () => {
    it('should call BudgetsService#update()', fakeAsync(() => {
      budgetsServiceSpy.update.withArgs('project_id', 'id', { description: 'une description' })
                       .and.returnValue(
        new Observable(
          (subscriber) => {
            subscriber.next({
              id: 'test', name: 'Budget', description: 'description',
              project_id: 'id', url: 'une URL',
              created_at: "2023-04-14T05:34:47.782Z", updated_at: "2023-04-14T05:34:47.782Z"
            } as IBudget);
          }
        )
      );

      service.update_budget('project_id', 'id', { description: 'une description' });

      expect(budgetsServiceSpy.update).toHaveBeenCalledTimes(1);
    }));
  });

  describe('#delete_budget', () => {
    it('should call BudgetsService#delete()', fakeAsync(() => {
      budgetsServiceSpy.delete.withArgs('project_id', 'id').and.returnValue(
        new Observable(
          (subscriber) => {
            subscriber.next(null);
          }
        )
      );

      service.delete_budget('project_id', 'id');

      expect(budgetsServiceSpy.delete).toHaveBeenCalledTimes(1);
    }));
  });

  /* Charges management tests */

  describe('#charges', () => {
    it('should call ChargesService#list()', fakeAsync(() => {
      chargesServiceSpy.list.withArgs('project_id').and.returnValue(
        new Observable(
          (subscriber) => {
            subscriber.next([
              {
                id: 'test', name: 'Charge', description: 'description',
                project_id: 'id', budget_id: null, frequency: EFrequencies.biweekly,
                amount: 29.9,
                url: 'une URL', created_at: "2023-04-14T05:34:47.782Z", updated_at: "2023-04-14T05:34:47.782Z"
              }
            ])
          }
        )
      );

      service.charges('project_id');

      expect(chargesServiceSpy.list).toHaveBeenCalledTimes(1)
    }));
  });

  describe('#charge', () => {
    it('should call ChargesService#show()', fakeAsync(() => {
      chargesServiceSpy.show.withArgs('project_id', 'id').and.returnValue(
        new Observable(
          (subscriber) => {
            subscriber.next({
              id: 'test', name: 'Charge', description: 'description',
              project_id: 'id', budget_id: null, frequency: EFrequencies.weekly,
              amount: 22,
              url: 'une URL', created_at: "2023-04-14T05:34:47.782Z", updated_at: "2023-04-14T05:34:47.782Z"
            });
          }
        )
      );

      service.charge('project_id', 'id');

      expect(chargesServiceSpy.show).toHaveBeenCalledTimes(1);
    }));
  });

  describe('#create_charge', () => {
    it('should call ChargesService#create()', fakeAsync(() => {
      chargesServiceSpy.create.withArgs(
        'project_id',
        {
          name: 'Ma Charge', budget_id: null, frequency: EFrequencies.annually,
          amount: 1, description: 'une description'
        }
      )
                       .and.returnValue(
        new Observable(
          (subscriber) => {
            subscriber.next({
              id: 'test', name: 'Ma Charge', description: 'description',
              project_id: 'id', budget_id: null, frequency: EFrequencies.annually,
              amount: 1,
              url: 'une URL', created_at: "2023-04-14T05:34:47.782Z", updated_at: "2023-04-14T05:34:47.782Z"
            });
          }
        )
      );

      service.create_charge(
        'project_id', {
          name: 'Ma Charge', budget_id: null, frequency: EFrequencies.annually,
          amount: 1, description: 'une description'
        }
      );

      expect(chargesServiceSpy.create).toHaveBeenCalledTimes(1);
    }));
  });

  describe('#update_charge', () => {
    it('should call ChargesService#update()', fakeAsync(() => {
      chargesServiceSpy.update.withArgs('project_id', 'id', { description: 'une description' })
                       .and.returnValue(
        new Observable(
          (subscriber) => {
            subscriber.next({
              id: 'test', name: 'Ma Charge', description: 'description',
              project_id: 'id', budget_id: null, frequency: EFrequencies.annually,
              amount: 1,
              url: 'une URL', created_at: "2023-04-14T05:34:47.782Z", updated_at: "2023-04-14T05:34:47.782Z"
            });
          }
        )
      );

      service.update_charge('project_id', 'id', { description: 'une description' });

      expect(chargesServiceSpy.update).toHaveBeenCalledTimes(1);
    }));
  });

  describe('#delete_charge', () => {
    it('should call ChargesService#delete()', fakeAsync(() => {
      chargesServiceSpy.delete.withArgs('project_id', 'id').and.returnValue(
        new Observable(
          (subscriber) => {
            subscriber.next(null);
          }
        )
      );

      service.delete_charge('project_id', 'id');

      expect(chargesServiceSpy.delete).toHaveBeenCalledTimes(1);
    }));
  });

  /* Projects management tests */

  describe('#projects', () => {
    it('should call ProjectsService#list()', fakeAsync(() => {
      projectsServiceSpy.list.withArgs().and.returnValue(
        new Observable(
          (subscriber) => {
            subscriber.next([
              {
                id: 'test', name: 'Charge', description: 'description',
                owner_id: 'id',
                url: 'une URL', created_at: "2023-04-14T05:34:47.782Z", updated_at: "2023-04-14T05:34:47.782Z"
              }
            ])
          }
        )
      );

      service.projects();

      expect(projectsServiceSpy.list).toHaveBeenCalledTimes(1)
    }));
  });

  describe('#project', () => {
    it('should call ProjectsService#show()', fakeAsync(() => {
      projectsServiceSpy.show.withArgs('id').and.returnValue(
        new Observable(
          (subscriber) => {
            subscriber.next({
              id: 'test', name: 'Charge', description: 'description',
              owner_id: 'id',
              url: 'une URL', created_at: "2023-04-14T05:34:47.782Z", updated_at: "2023-04-14T05:34:47.782Z"
            });
          }
        )
      );

      service.project('id');

      expect(projectsServiceSpy.show).toHaveBeenCalledTimes(1);
    }));
  });

  /* Units management tests */

  describe('#units', () => {
    it('should call UnitsService#list()', fakeAsync(() => {
      unitsServiceSpy.list.withArgs().and.returnValue(
        new Observable(
          (subscriber) => {
            subscriber.next([
              {
                id: 'test', name: 'Unit', description: 'description',
                creator_id: 'id',
                url: 'une URL', created_at: "2023-04-14T05:34:47.782Z", updated_at: "2023-04-14T05:34:47.782Z"
              }
            ])
          }
        )
      );

      service.units();

      expect(unitsServiceSpy.list).toHaveBeenCalledTimes(1)
    }));
  });

  describe('#unit', () => {
    it('should call UnitsService#show()', fakeAsync(() => {
      unitsServiceSpy.show.withArgs('id').and.returnValue(
        new Observable(
          (subscriber) => {
            subscriber.next({
              id: 'test', name: 'Unit', description: 'description',
              creator_id: null,
              url: 'une URL', created_at: "2023-04-14T05:34:47.782Z", updated_at: "2023-04-14T05:34:47.782Z"
            });
          }
        )
      );

      service.unit('id');

      expect(unitsServiceSpy.show).toHaveBeenCalledTimes(1);
    }));
  });

  describe('#create_unit', () => {
    it('should call UnitsService#create()', fakeAsync(() => {
      unitsServiceSpy.create.withArgs({
        name: 'Mon unité', creator_id: 'id', description: 'une description'
      })
                       .and.returnValue(
        new Observable(
          (subscriber) => {
            subscriber.next({
              id: 'test', name: 'Mon unité', description: 'description',
              creator_id: 'id',
              url: 'une URL', created_at: "2023-04-14T05:34:47.782Z", updated_at: "2023-04-14T05:34:47.782Z"
            });
          }
        )
      );

      service.create_unit({
        name: 'Mon unité', creator_id: 'id', description: 'une description'
      });

      expect(unitsServiceSpy.create).toHaveBeenCalledTimes(1);
    }));
  });

  describe('#update_unit', () => {
    it('should call UnitsService#update()', fakeAsync(() => {
      unitsServiceSpy.update.withArgs('id', { description: 'une description' })
                       .and.returnValue(
        new Observable(
          (subscriber) => {
            subscriber.next({
              id: 'test', name: 'Mon unité', creator_id: 'id', description: 'description',
              url: 'une URL', created_at: "2023-04-14T05:34:47.782Z", updated_at: "2023-04-14T05:34:47.782Z"
            });
          }
        )
      );

      service.update_unit('id', { description: 'une description' });

      expect(unitsServiceSpy.update).toHaveBeenCalledTimes(1);
    }));
  });

  describe('#delete_unit', () => {
    it('should call UnitsService#delete()', fakeAsync(() => {
      unitsServiceSpy.delete.withArgs('id').and.returnValue(
        new Observable(
          (subscriber) => {
            subscriber.next(null);
          }
        )
      );

      service.delete_unit('id');

      expect(unitsServiceSpy.delete).toHaveBeenCalledTimes(1);
    }));
  });

  /* Charges management tests */

  describe('#products', () => {
    it('should call ProductsService#list()', fakeAsync(() => {
      productsServiceSpy.list.withArgs('project_id').and.returnValue(
        new Observable(
          (subscriber) => {
            subscriber.next([
              {
                id: 'id', name: 'Product', description: 'description',
                project_id: 'project_id', selling_unit_id: 'id2', public_price: 29.9,
                url: 'une URL', created_at: "2023-04-14T05:34:47.782Z", updated_at: "2023-04-14T05:34:47.782Z"
              }
            ])
          }
        )
      );

      service.products('project_id');

      expect(productsServiceSpy.list).toHaveBeenCalledTimes(1)
    }));
  });

  describe('#product', () => {
    it('should call ProductsService#show()', fakeAsync(() => {
      productsServiceSpy.show.withArgs('project_id', 'id').and.returnValue(
        new Observable(
          (subscriber) => {
            subscriber.next({
              id: 'id', name: 'Charge', description: 'description',
              project_id: 'project_id', selling_unit_id: 'id2', public_price: 22,
              url: 'une URL', created_at: "2023-04-14T05:34:47.782Z", updated_at: "2023-04-14T05:34:47.782Z"
            });
          }
        )
      );

      service.product('project_id', 'id');

      expect(productsServiceSpy.show).toHaveBeenCalledTimes(1);
    }));
  });

  describe('#create_product', () => {
    it('should call ProductsService#create()', fakeAsync(() => {
      productsServiceSpy.create.withArgs(
        'project_id',
        {
          name: 'Mon Product', selling_unit_id: 'id2', public_price: 1, description: 'une description'
        }
      )
                       .and.returnValue(
        new Observable(
          (subscriber) => {
            subscriber.next({
              id: 'test', name: 'Ma Charge', description: 'description',
              project_id: 'project_id', selling_unit_id: 'id2', public_price: 1,
              url: 'une URL', created_at: "2023-04-14T05:34:47.782Z", updated_at: "2023-04-14T05:34:47.782Z"
            });
          }
        )
      );

      service.create_product(
        'project_id', {
          name: 'Mon Product', selling_unit_id: 'id2', public_price: 1, description: 'une description'
        }
      );

      expect(productsServiceSpy.create).toHaveBeenCalledTimes(1);
    }));
  });

  describe('#update_product', () => {
    it('should call ProductsService#update()', fakeAsync(() => {
      productsServiceSpy.update.withArgs('project_id', 'id', { description: 'une description' })
                       .and.returnValue(
        new Observable(
          (subscriber) => {
            subscriber.next({
              id: 'id', name: 'Mon Produit', description: 'description',
              project_id: 'project_id', selling_unit_id: 'id2', public_price: 1,
              url: 'une URL', created_at: "2023-04-14T05:34:47.782Z", updated_at: "2023-04-14T05:34:47.782Z"
            });
          }
        )
      );

      service.update_product('project_id', 'id', { description: 'une description' });

      expect(productsServiceSpy.update).toHaveBeenCalledTimes(1);
    }));
  });

  describe('#delete_product', () => {
    it('should call ProductsService#delete()', fakeAsync(() => {
      productsServiceSpy.delete.withArgs('project_id', 'id').and.returnValue(
        new Observable(
          (subscriber) => {
            subscriber.next(null);
          }
        )
      );

      service.delete_product('project_id', 'id');

      expect(productsServiceSpy.delete).toHaveBeenCalledTimes(1);
    }));
  });

  /* Charges management tests */

  describe('#services', () => {
    it('should call ServicesService#list()', fakeAsync(() => {
      servicesServiceSpy.list.withArgs('project_id').and.returnValue(
        new Observable(
          (subscriber) => {
            subscriber.next([
              {
                id: 'id', name: 'Charge', description: 'description',
                project_id: 'project_id', selling_unit_id: 'id2', public_price: 29.9,
                url: 'une URL', created_at: "2023-04-14T05:34:47.782Z", updated_at: "2023-04-14T05:34:47.782Z"
              }
            ])
          }
        )
      );

      service.services('project_id');

      expect(servicesServiceSpy.list).toHaveBeenCalledTimes(1)
    }));
  });

  describe('#service', () => {
    it('should call ServicesService#show()', fakeAsync(() => {
      servicesServiceSpy.show.withArgs('project_id', 'id').and.returnValue(
        new Observable(
          (subscriber) => {
            subscriber.next({
              id: 'id', name: 'Charge', description: 'description',
              project_id: 'project_id', selling_unit_id: 'id2', public_price: 22,
              url: 'une URL', created_at: "2023-04-14T05:34:47.782Z", updated_at: "2023-04-14T05:34:47.782Z"
            });
          }
        )
      );

      service.service('project_id', 'id');

      expect(servicesServiceSpy.show).toHaveBeenCalledTimes(1);
    }));
  });

  describe('#create_service', () => {
    it('should call ServicesService#create()', fakeAsync(() => {
      servicesServiceSpy.create.withArgs(
        'project_id',
        {
          name: 'Mon Service', selling_unit_id: 'id2', public_price: 1, description: 'une description'
        }
      )
                       .and.returnValue(
        new Observable(
          (subscriber) => {
            subscriber.next({
              id: 'test', name: 'Mon service', description: 'description',
              project_id: 'id', selling_unit_id: 'id2', public_price: 1,
              url: 'une URL', created_at: "2023-04-14T05:34:47.782Z", updated_at: "2023-04-14T05:34:47.782Z"
            });
          }
        )
      );

      service.create_service(
        'project_id', {
          name: 'Mon Service', selling_unit_id: 'id2', public_price: 1, description: 'une description'
        }
      );

      expect(servicesServiceSpy.create).toHaveBeenCalledTimes(1);
    }));
  });

  describe('#update_service', () => {
    it('should call ServicesService#update()', fakeAsync(() => {
      servicesServiceSpy.update.withArgs('project_id', 'id', { description: 'une description' })
                       .and.returnValue(
        new Observable(
          (subscriber) => {
            subscriber.next({
              id: 'test', name: 'Mon Service', description: 'description',
              project_id: 'project_id', selling_unit_id: 'id3', public_price: 1,
              url: 'une URL', created_at: "2023-04-14T05:34:47.782Z", updated_at: "2023-04-14T05:34:47.782Z"
            });
          }
        )
      );

      service.update_service('project_id', 'id', { description: 'une description' });

      expect(servicesServiceSpy.update).toHaveBeenCalledTimes(1);
    }));
  });

  describe('#delete_service', () => {
    it('should call ServicesService#delete()', fakeAsync(() => {
      servicesServiceSpy.delete.withArgs('project_id', 'id').and.returnValue(
        new Observable(
          (subscriber) => {
            subscriber.next(null);
          }
        )
      );

      service.delete_service('project_id', 'id');

      expect(servicesServiceSpy.delete).toHaveBeenCalledTimes(1);
    }));
  });
});
