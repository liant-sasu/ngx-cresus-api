
export * from './models/budget';
export * from './models/charge';
export * from './models/project';
export * from './models/unit';
export * from './models/user';
export * from './models/product';
export * from './models/service';

export interface IModels {};

export interface IVersion extends IModels {
  version: string;
}

export interface IError extends IModels {
  code: Number;
  message: string;
}