import { TestBed, fakeAsync, waitForAsync } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing'

import { CommunicateService } from './communicate.service';

describe('CommunicateService', () => {
  let service: CommunicateService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ]
    })
           .compileComponents();
    service = TestBed.inject(CommunicateService);
  });
  
  let httpTestingController: HttpTestingController;
  beforeEach(() => httpTestingController = TestBed.inject(HttpTestingController));
  
  afterEach(() => {
      httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#build_url', () => {
    it('should insert default api_version when none specified', () =>{
      expect(service.build_url('toto')).toEqual('https://cresus-api.liant.dev/v0/toto');
    });
    it('should not insert default api_version when null specified', () =>{
      expect(service.build_url('toto', null)).toEqual('https://cresus-api.liant.dev/toto');
    });
    it('should insert the api_version specified', () =>{
      expect(service.build_url('toto', 'v1')).toEqual('https://cresus-api.liant.dev/v1/toto');
    });
  });

  describe('#get', () => {
    it('should make a GET request to asked url without api_version when path is "version"', () =>{
      service.get('version').subscribe(
        (data) => expect(data).toEqual({ version: '0.0.1' })
      );

      const req = httpTestingController.expectOne(('https://cresus-api.liant.dev/version'));
      req.flush({ version: '0.0.1' });
    });
    it('should make a GET request to asked url with api_version', () =>{
      service.get('toto').subscribe(
        (data) => expect(data).toEqual({})
      );

      const req = httpTestingController.expectOne(('https://cresus-api.liant.dev/v0/toto'));
      req.flush({});
    });
  });

  describe('#post', () => {
    it('should make a POST request to asked url with api_version and body', () =>{
      service.post('toto', { mon: "payload" }).subscribe(
        (data) => expect(data).toEqual({ ma: 'réponse' })
      );

      const req = httpTestingController.expectOne({
        url: 'https://cresus-api.liant.dev/v0/toto', method: 'post',
        
      });
      expect(JSON.parse(req.request.body)).toEqual({ "mon": "payload" });
      req.flush({ ma: 'réponse' });
    });
  });

  describe('#patch', () => {
    it('should make a PATCH request to asked url with api_version and body', () =>{
      service.patch('toto/1', { mon: "payload" }).subscribe(
        (data) => expect(data).toEqual({ ma: 'réponse' })
      );

      const req = httpTestingController.expectOne({
        url: 'https://cresus-api.liant.dev/v0/toto/1', method: 'patch',
        
      });
      expect(JSON.parse(req.request.body)).toEqual({ "mon": "payload" });
      req.flush({ ma: 'réponse' });
    });
  });

  describe('#delete', () => {
    it('should make a DELETE request to asked url with api_version', () =>{
      service.delete('toto/1').subscribe(
        (data) => expect(data).toEqual(null)
      );

      const req = httpTestingController.expectOne({
        url: 'https://cresus-api.liant.dev/v0/toto/1', method: 'DELETE'
      });
      req.flush(null);
    });
  });
});
