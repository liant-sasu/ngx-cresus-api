import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CommunicateService } from '../communicate.service';
import { IBudget, IBudgetUpdate, IBudgetCreate } from '../models';

@Injectable({
  providedIn: 'root'
})
export class BudgetsService {

  constructor(private com_service: CommunicateService) { }

  list(project_id: string): Observable<Array<IBudget>> {
    return this.com_service.get<Array<IBudget>>(`projects/${project_id}/budgets`);
  }

  show(project_id: string, id: string): Observable<IBudget> {
    return this.com_service.get<IBudget>(`projects/${project_id}/budgets/${id}`);
  }

  update(project_id: string, id: string, body: IBudgetUpdate): Observable<IBudget> {
    return this.com_service.patch<IBudget>(
      `projects/${project_id}/budgets/${id}`, { budget: body }
      );
  }

  create(project_id: string, n_unit: IBudgetCreate): Observable<IBudget> {
    return this.com_service.post(`projects/${project_id}/budgets`, { budget: n_unit });
  }

  delete(project_id: string, id: string): Observable<null> {
    return this.com_service.delete(`projects/${project_id}/budgets/${id}`);
  }
}
