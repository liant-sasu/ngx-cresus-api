import { Injectable } from '@angular/core';
import { CommunicateService } from '../communicate.service';
import { Observable } from 'rxjs';
import { IProject } from '../models';

@Injectable({
  providedIn: 'root'
})
export class ProjectsService {

  constructor(private com_service: CommunicateService) { }

  list(): Observable<Array<IProject>> {
    return this.com_service.get<Array<IProject>>('projects');
  }

  show(id: string): Observable<IProject> {
    return this.com_service.get<IProject>(`projects/${id}`);
  }
}
