import { Injectable } from '@angular/core';
import { CommunicateService } from '../communicate.service';
import { IUnit, IUnitCreate, IUnitUpdate } from '../models';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class UnitsService {

  constructor(private com_service: CommunicateService) { }

  list(): Observable<Array<IUnit>> {
    return this.com_service.get<Array<IUnit>>('units');
  }

  show(id: string): Observable<IUnit> {
    return this.com_service.get<IUnit>(`units/${id}`);
  }

  update(id: string, body: IUnitUpdate): Observable<IUnit> {
    return this.com_service.patch<IUnit>(
      `units/${id}`, { unit: body }
      );
  }

  create(n_unit: IUnitCreate): Observable<IUnit> {
    return this.com_service.post('units', { unit: n_unit });
  }

  delete(id: string): Observable<null> {
    return this.com_service.delete(`units/${id}`);
  }
}
