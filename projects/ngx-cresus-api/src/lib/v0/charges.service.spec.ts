import { TestBed, fakeAsync, waitForAsync } from '@angular/core/testing';

import { ChargesService } from './charges.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { CommunicateService } from '../communicate.service';
import { EFrequencies, ICharge } from '../models';

describe('ChargesService', () => {
  let service: ChargesService,
      httpTestingController: HttpTestingController,
      comService: CommunicateService;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ]
    });
    service = TestBed.inject(ChargesService);
    httpTestingController = TestBed.inject(HttpTestingController);
    comService = TestBed.inject(CommunicateService);
  }));

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#list', () => {
    it('should get available charges', fakeAsync(() => {
      let myCharges: Array<ICharge> = [
        {
          "id": "6041fead-7cc9-483a-8937-a860e35aa910",
          "name": "Charge 1",
          "project_id": "1c2f73e5-997c-4c93-aa81-c86fe15fa294",
          "budget_id": null,
          "frequency": EFrequencies.monthly,
          "amount": 29.9,
          "description": "Une première charge",
          "created_at": "2023-04-14T05:34:47.782Z",
          "updated_at": "2023-04-14T05:34:47.782Z",
          "url": "string"
        },
        {
          "id": "a440e9e6-8cb4-4cf0-9182-d1bbfbd8ed47",
          "name": "Charge 2",
          "project_id": "1c2f73e5-997c-4c93-aa81-c86fe15fa294",
          "budget_id": "1c2f73e5-997c-4c93-aa81-c86fe15fa294",
          "frequency": EFrequencies.bimonthly,
          "amount": 60,
          "description": "Une deuxième charge",
          "created_at": "2023-04-14T05:34:47.782Z",
          "updated_at": "2023-04-14T05:34:47.782Z",
          "url": "string"
        }
      ];
      service.list("1c2f73e5-997c-4c93-aa81-c86fe15fa294").subscribe(
        function(charges: Array<ICharge>) {
          expect(charges.length).toEqual(2)
          expect(charges[0].id).toEqual(myCharges[0].id)
        }
      );

      const req = httpTestingController.expectOne({
        url: comService.build_url('projects/1c2f73e5-997c-4c93-aa81-c86fe15fa294/charges'), method: 'get'
      });
      req.flush(myCharges);
    }));
  });

  describe('#show', () => {
    it('should get a charge information from server', fakeAsync(() => {
      let myCharge = {
        "id": "6041fead-7cc9-483a-8937-a860e35aa910",
        "name": "Charge 1",
        "project_id": "1c2f73e5-997c-4c93-aa81-c86fe15fa294",
        "budget_id": null,
        "frequency": EFrequencies.monthly,
        "amount": 29.9,
        "description": "Une première charge",
        "created_at": "2023-04-14T05:34:47.782Z",
        "updated_at": "2023-04-14T05:34:47.782Z",
        "url": "string"
      };
      service.show("1c2f73e5-997c-4c93-aa81-c86fe15fa294", myCharge.id).subscribe(
        function(charge: ICharge) {
          expect(charge).toEqual(myCharge)
        }
      );

      const req = httpTestingController.expectOne(comService.build_url(`projects/1c2f73e5-997c-4c93-aa81-c86fe15fa294/charges/${myCharge.id}`));
      req.flush(myCharge);
    }));
  });

  describe('#create', () => {
    it('should post new charge information to server', fakeAsync(() => {
      let n_charge = {
            "name": "Charge 1",
            "budget_id": null,
            "frequency": EFrequencies.monthly,
            "amount": 29.9,
            "description": "Une première charge",
          },
          myCharge = {
            ...n_charge,
            "id": "6041fead-7cc9-483a-8937-a860e35aa910",
            "project_id": "1c2f73e5-997c-4c93-aa81-c86fe15fa294",
            "url": "string"
          };
      service.create("1c2f73e5-997c-4c93-aa81-c86fe15fa294", n_charge).subscribe(function(_charge: ICharge) {});

      const req = httpTestingController.expectOne({
        url: comService.build_url('projects/1c2f73e5-997c-4c93-aa81-c86fe15fa294/charges'),
        method: 'post'
      });
      expect(JSON.parse(req.request.body)).toEqual({'charge': n_charge })
      req.flush(myCharge);
    }));
  });

  describe('#delete', () => {
    it('should generate a delete request on the good charge', fakeAsync(() => {
      service.delete("1c2f73e5-997c-4c93-aa81-c86fe15fa294", "6041fead-7cc9-483a-8937-a860e35aa910")
             .subscribe(function(data: null) {
              expect(data).toBe(null);
             });
  
      const req = httpTestingController.expectOne({
        url: comService.build_url('projects/1c2f73e5-997c-4c93-aa81-c86fe15fa294/charges/6041fead-7cc9-483a-8937-a860e35aa910'),
        method: 'delete'
      });
      req.flush(null);
    }));
  });

  describe('#update', () => {
    it('should patch charge with the update information to server', fakeAsync(() => {
      let to_update = {
            "description": "Une première charge."
          },
          myCharge = {
            "id": "6041fead-7cc9-483a-8937-a860e35aa910",
            "name": "Charge 1",
            "project_id": "1c2f73e5-997c-4c93-aa81-c86fe15fa294",
            "budget_id": null,
            "frequency": EFrequencies.monthly,
            "amount": 29.9,
            "description": "Une première charge",
            "created_at": "2023-04-14T05:34:47.782Z",
            "updated_at": "2023-04-14T05:34:47.782Z",
            "url": "string"
          };
      service.update(myCharge.project_id, myCharge.id, to_update)
             .subscribe(function(_charge: ICharge) {});

      const req = httpTestingController.expectOne({
        url: comService.build_url(`projects/${myCharge.project_id}/charges/${myCharge.id}`),
        method: 'patch'
      });
      expect(JSON.parse(req.request.body)).toEqual({ 'charge': to_update })
      req.flush(myCharge);
    }));
  });
});
