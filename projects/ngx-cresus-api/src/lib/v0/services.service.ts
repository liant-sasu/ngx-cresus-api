import { Injectable } from '@angular/core';
import { CommunicateService } from '../communicate.service';
import { IService, IServiceUpdate, IServiceCreate } from '../models';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class ServicesService {

  constructor(private com_service: CommunicateService) { }

  list(project_id: string): Observable<Array<IService>> {
    return this.com_service.get<Array<IService>>(`projects/${project_id}/services`);
  }

  show(project_id: string, id: string): Observable<IService> {
    return this.com_service.get<IService>(`projects/${project_id}/services/${id}`);
  }

  update(project_id: string, id: string, body: IServiceUpdate): Observable<IService> {
    return this.com_service.patch<IService>(
      `projects/${project_id}/services/${id}`, { service: body }
      );
  }

  create(project_id: string, n_unit: IServiceCreate): Observable<IService> {
    return this.com_service.post(`projects/${project_id}/services`, { service: n_unit });
  }

  delete(project_id: string, id: string): Observable<null> {
    return this.com_service.delete(`projects/${project_id}/services/${id}`);
  }
}
