import { TestBed, fakeAsync, waitForAsync } from '@angular/core/testing';

import { ProductsService } from './products.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { CommunicateService } from '../communicate.service';
import { IProduct } from '../models';

describe('ProductsService', () => {
  let service: ProductsService,
      httpTestingController: HttpTestingController,
      comService: CommunicateService;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ]
    });
    service = TestBed.inject(ProductsService);
    httpTestingController = TestBed.inject(HttpTestingController);
    comService = TestBed.inject(CommunicateService);
  }));

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#list', () => {
    it('should get available products', fakeAsync(() => {
      let myProducts: Array<IProduct> = [
        {
          "id": "6041fead-7cc9-483a-8937-a860e35aa910",
          "name": "Product 1",
          "project_id": "1c2f73e5-997c-4c93-aa81-c86fe15fa294",
          "selling_unit_id": "1c2f73e5-997c-4c93-aa81-c86fe15fa294",
          "public_price": 29.9,
          "description": "Un premier product",
          "created_at": "2023-04-14T05:34:47.782Z",
          "updated_at": "2023-04-14T05:34:47.782Z",
          "url": "string"
        },
        {
          "id": "a440e9e6-8cb4-4cf0-9182-d1bbfbd8ed47",
          "name": "Product 2",
          "project_id": "1c2f73e5-997c-4c93-aa81-c86fe15fa294",
          "selling_unit_id": "1c2f73e5-997c-4c93-aa81-c86fe15fa294",
          "public_price": 60,
          "description": "Un deuxième product",
          "created_at": "2023-04-14T05:34:47.782Z",
          "updated_at": "2023-04-14T05:34:47.782Z",
          "url": "string"
        }
      ];
      service.list("1c2f73e5-997c-4c93-aa81-c86fe15fa294").subscribe(
        function(products: Array<IProduct>) {
          expect(products.length).toEqual(2)
          expect(products[0].id).toEqual(myProducts[0].id)
        }
      );

      const req = httpTestingController.expectOne({
        url: comService.build_url('projects/1c2f73e5-997c-4c93-aa81-c86fe15fa294/products'), method: 'get'
      });
      req.flush(myProducts);
    }));
  });

  describe('#show', () => {
    it('should get a product information from server', fakeAsync(() => {
      let myProduct = {
        "id": "6041fead-7cc9-483a-8937-a860e35aa910",
        "name": "Product 1",
        "project_id": "1c2f73e5-997c-4c93-aa81-c86fe15fa294",
        "selling_unit_id": "1c2f73e5-997c-4c93-aa81-c86fe15fa294",
        "public_price": 29.9,
        "description": "Un premier product",
        "created_at": "2023-04-14T05:34:47.782Z",
        "updated_at": "2023-04-14T05:34:47.782Z",
        "url": "string"
      };
      service.show("1c2f73e5-997c-4c93-aa81-c86fe15fa294", myProduct.id).subscribe(
        function(product: IProduct) {
          expect(product).toEqual(myProduct)
        }
      );

      const req = httpTestingController.expectOne(comService.build_url(`projects/1c2f73e5-997c-4c93-aa81-c86fe15fa294/products/${myProduct.id}`));
      req.flush(myProduct);
    }));
  });

  describe('#create', () => {
    it('should post new product information to server', fakeAsync(() => {
      let n_product = {
            "name": "Product 1",
            "selling_unit_id": "1c2f73e5-997c-4c93-aa81-c86fe15fa294",
            "public_price": 29.9,
            "description": "Un premier product",
          },
          myProduct = {
            ...n_product,
            "id": "6041fead-7cc9-483a-8937-a860e35aa910",
            "project_id": "1c2f73e5-997c-4c93-aa81-c86fe15fa294",
            "url": "string"
          };
      service.create("1c2f73e5-997c-4c93-aa81-c86fe15fa294", n_product).subscribe(function(_product: IProduct) {});

      const req = httpTestingController.expectOne({
        url: comService.build_url('projects/1c2f73e5-997c-4c93-aa81-c86fe15fa294/products'),
        method: 'post'
      });
      expect(JSON.parse(req.request.body)).toEqual({'product': n_product })
      req.flush(myProduct);
    }));
  });

  describe('#delete', () => {
    it('should generate a delete request on the good product', fakeAsync(() => {
      service.delete("1c2f73e5-997c-4c93-aa81-c86fe15fa294", "6041fead-7cc9-483a-8937-a860e35aa910")
             .subscribe(function(data: null) {
              expect(data).toBe(null);
             });
  
      const req = httpTestingController.expectOne({
        url: comService.build_url('projects/1c2f73e5-997c-4c93-aa81-c86fe15fa294/products/6041fead-7cc9-483a-8937-a860e35aa910'),
        method: 'delete'
      });
      req.flush(null);
    }));
  });

  describe('#update', () => {
    it('should patch product with the update information to server', fakeAsync(() => {
      let to_update = {
            "description": "Un premier product."
          },
          myProduct = {
            "id": "6041fead-7cc9-483a-8937-a860e35aa910",
            "name": "Product 1",
            "project_id": "1c2f73e5-997c-4c93-aa81-c86fe15fa294",
            "selling_unit_id": "1c2f73e5-997c-4c93-aa81-c86fe15fa294",
            "public_price": 29.9,
            "description": "Un premier product",
            "created_at": "2023-04-14T05:34:47.782Z",
            "updated_at": "2023-04-14T05:34:47.782Z",
            "url": "string"
          };
      service.update(myProduct.project_id, myProduct.id, to_update)
             .subscribe(function(_product: IProduct) {});

      const req = httpTestingController.expectOne({
        url: comService.build_url(`projects/${myProduct.project_id}/products/${myProduct.id}`),
        method: 'patch'
      });
      expect(JSON.parse(req.request.body)).toEqual({ 'product': to_update })
      req.flush(myProduct);
    }));
  });
});
