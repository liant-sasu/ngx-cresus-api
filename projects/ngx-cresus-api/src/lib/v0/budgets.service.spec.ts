import { TestBed, fakeAsync, waitForAsync } from '@angular/core/testing';

import { BudgetsService } from './budgets.service';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { CommunicateService } from '../communicate.service';
import { IBudget } from '../models';

describe('BudgetsService', () => {
  let service: BudgetsService,
      httpTestingController: HttpTestingController,
      comService: CommunicateService;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ]
    });
    service = TestBed.inject(BudgetsService);
    httpTestingController = TestBed.inject(HttpTestingController);
    comService = TestBed.inject(CommunicateService);
  }));

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#list', () => {
    it('should get available budgets', fakeAsync(() => {
      let myBudgets: IBudget[] = [
        {
          "id": "6041fead-7cc9-483a-8937-a860e35aa910",
          "name": "Budget 1",
          "project_id": "1c2f73e5-997c-4c93-aa81-c86fe15fa294",
          "description": "Un premier budget",
          "created_at": "2023-04-14T05:34:47.782Z",
          "updated_at": "2023-04-14T05:34:47.782Z",
          "url": "string"
        },
        {
          "id": "a440e9e6-8cb4-4cf0-9182-d1bbfbd8ed47",
          "name": "Budget 2",
          "project_id": "1c2f73e5-997c-4c93-aa81-c86fe15fa294",
          "description": "Un deuxième budget",
          "created_at": "2023-04-14T05:34:47.782Z",
          "updated_at": "2023-04-14T05:34:47.782Z",
          "url": "string"
        }
      ];
      service.list("1c2f73e5-997c-4c93-aa81-c86fe15fa294").subscribe(
        function(budgets: IBudget[]) {
          expect(budgets.length).toEqual(2)
          expect(budgets[0].id).toEqual(myBudgets[0].id)
        }
      );

      const req = httpTestingController.expectOne({
        url: comService.build_url('projects/1c2f73e5-997c-4c93-aa81-c86fe15fa294/budgets'), method: 'get'
      });
      req.flush(myBudgets);
    }));
  });

  describe('#show', () => {
    it('should get a budget information from server', fakeAsync(() => {
      let myBudget = {
        "id": "6041fead-7cc9-483a-8937-a860e35aa910",
        "name": "Budget 1",
        "project_id": "1c2f73e5-997c-4c93-aa81-c86fe15fa294",
        "description": "Une première budget",
        "created_at": "2023-04-14T05:34:47.782Z",
        "updated_at": "2023-04-14T05:34:47.782Z",
        "url": "string"
      };
      service.show("1c2f73e5-997c-4c93-aa81-c86fe15fa294", myBudget.id).subscribe(
        function(budget: IBudget) {
          expect(budget).toEqual(myBudget)
        }
      );

      const req = httpTestingController.expectOne(comService.build_url(`projects/1c2f73e5-997c-4c93-aa81-c86fe15fa294/budgets/${myBudget.id}`));
      req.flush(myBudget);
    }));
  });

  describe('#create', () => {
    it('should post new budget information to server', fakeAsync(() => {
      let n_budget = {
            "name": "Budget 1",
            "description": "Un premier budget",
          },
          myBudget = {
            ...n_budget,
            "id": "6041fead-7cc9-483a-8937-a860e35aa910",
            "project_id": "1c2f73e5-997c-4c93-aa81-c86fe15fa294",
            "url": "string"
          };
      service.create("1c2f73e5-997c-4c93-aa81-c86fe15fa294", n_budget).subscribe(function(_budget: IBudget) {});

      const req = httpTestingController.expectOne({
        url: comService.build_url('projects/1c2f73e5-997c-4c93-aa81-c86fe15fa294/budgets'),
        method: 'post'
      });
      expect(JSON.parse(req.request.body)).toEqual({'budget': n_budget })
      req.flush(myBudget);
    }));
  });

  describe('#delete', () => {
    it('should generate a delete request on the good budget', fakeAsync(() => {
      service.delete("1c2f73e5-997c-4c93-aa81-c86fe15fa294", "6041fead-7cc9-483a-8937-a860e35aa910")
             .subscribe(function(data: null) {
              expect(data).toBe(null);
             });
  
      const req = httpTestingController.expectOne({
        url: comService.build_url('projects/1c2f73e5-997c-4c93-aa81-c86fe15fa294/budgets/6041fead-7cc9-483a-8937-a860e35aa910'),
        method: 'delete'
      });
      req.flush(null);
    }));
  });

  describe('#update', () => {
    it('should patch budget with the update information to server', fakeAsync(() => {
      let to_update = {
            "description": "un premier budget."
          },
          myBudget = {
            "id": "6041fead-7cc9-483a-8937-a860e35aa910",
            "name": "Budget 1",
            "project_id": "1c2f73e5-997c-4c93-aa81-c86fe15fa294",
            "description": "Un premier budget",
            "created_at": "2023-04-14T05:34:47.782Z",
            "updated_at": "2023-04-14T05:34:47.782Z",
            "url": "string"
          };
      service.update(myBudget.project_id, myBudget.id, to_update)
             .subscribe(function(_budget: IBudget) {});

      const req = httpTestingController.expectOne({
        url: comService.build_url(`projects/${myBudget.project_id}/budgets/${myBudget.id}`),
        method: 'patch'
      });
      expect(JSON.parse(req.request.body)).toEqual({ 'budget': to_update })
      req.flush(myBudget);
    }));
  });
});
