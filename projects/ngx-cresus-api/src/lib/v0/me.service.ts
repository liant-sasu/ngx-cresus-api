import { Injectable } from '@angular/core';
import { CommunicateService } from '../communicate.service';
import { IUser } from '../models';

@Injectable({
  providedIn: 'root'
})
export class MeService {

  constructor(private com_service: CommunicateService) { }

  show() {
    return this.com_service.get<IUser>('me');
  }
}
