import { Injectable } from '@angular/core';
import { CommunicateService } from '../communicate.service';
import { IProduct, IProductUpdate, IProductCreate } from '../models';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(private com_service: CommunicateService) { }

  list(project_id: string): Observable<Array<IProduct>> {
    return this.com_service.get<Array<IProduct>>(`projects/${project_id}/products`);
  }

  show(project_id: string, id: string): Observable<IProduct> {
    return this.com_service.get<IProduct>(`projects/${project_id}/products/${id}`);
  }

  update(project_id: string, id: string, body: IProductUpdate): Observable<IProduct> {
    return this.com_service.patch<IProduct>(
      `projects/${project_id}/products/${id}`, { product: body }
      );
  }

  create(project_id: string, n_unit: IProductCreate): Observable<IProduct> {
    return this.com_service.post(`projects/${project_id}/products`, { product: n_unit });
  }

  delete(project_id: string, id: string): Observable<null> {
    return this.com_service.delete(`projects/${project_id}/products/${id}`);
  }
}
