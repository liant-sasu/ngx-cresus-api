import { Injectable } from '@angular/core';
import { CommunicateService } from '../communicate.service';
import { ICharge, IChargeUpdate, IChargeCreate } from '../models';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class ChargesService {

  constructor(private com_service: CommunicateService) { }

  list(project_id: string): Observable<Array<ICharge>> {
    return this.com_service.get<Array<ICharge>>(`projects/${project_id}/charges`);
  }

  show(project_id: string, id: string): Observable<ICharge> {
    return this.com_service.get<ICharge>(`projects/${project_id}/charges/${id}`);
  }

  update(project_id: string, id: string, body: IChargeUpdate): Observable<ICharge> {
    return this.com_service.patch<ICharge>(
      `projects/${project_id}/charges/${id}`, { charge: body }
      );
  }

  create(project_id: string, n_unit: IChargeCreate): Observable<ICharge> {
    return this.com_service.post(`projects/${project_id}/charges`, { charge: n_unit });
  }

  delete(project_id: string, id: string): Observable<null> {
    return this.com_service.delete(`projects/${project_id}/charges/${id}`);
  }
}
