import { TestBed, fakeAsync, waitForAsync } from '@angular/core/testing';

import { ServicesService } from './services.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { CommunicateService } from '../communicate.service';
import { IService } from '../models';

describe('ServicesService', () => {
  let service: ServicesService,
      httpTestingController: HttpTestingController,
      comService: CommunicateService;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ]
    });
    service = TestBed.inject(ServicesService);
    httpTestingController = TestBed.inject(HttpTestingController);
    comService = TestBed.inject(CommunicateService);
  }));

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#list', () => {
    it('should get available services', fakeAsync(() => {
      let myServices: Array<IService> = [
        {
          "id": "6041fead-7cc9-483a-8937-a860e35aa910",
          "name": "Service 1",
          "project_id": "1c2f73e5-997c-4c93-aa81-c86fe15fa294",
          "selling_unit_id": "1c2f73e5-997c-4c93-aa81-c86fe15fa294",
          "public_price": 29.9,
          "description": "Un premier service",
          "created_at": "2023-04-14T05:34:47.782Z",
          "updated_at": "2023-04-14T05:34:47.782Z",
          "url": "string"
        },
        {
          "id": "a440e9e6-8cb4-4cf0-9182-d1bbfbd8ed47",
          "name": "Service 2",
          "project_id": "1c2f73e5-997c-4c93-aa81-c86fe15fa294",
          "selling_unit_id": "1c2f73e5-997c-4c93-aa81-c86fe15fa294",
          "public_price": 60,
          "description": "Un deuxième service",
          "created_at": "2023-04-14T05:34:47.782Z",
          "updated_at": "2023-04-14T05:34:47.782Z",
          "url": "string"
        }
      ];
      service.list("1c2f73e5-997c-4c93-aa81-c86fe15fa294").subscribe(
        function(services: Array<IService>) {
          expect(services.length).toEqual(2)
          expect(services[0].id).toEqual(myServices[0].id)
        }
      );

      const req = httpTestingController.expectOne({
        url: comService.build_url('projects/1c2f73e5-997c-4c93-aa81-c86fe15fa294/services'), method: 'get'
      });
      req.flush(myServices);
    }));
  });

  describe('#show', () => {
    it('should get a service information from server', fakeAsync(() => {
      let myService = {
        "id": "6041fead-7cc9-483a-8937-a860e35aa910",
        "name": "Service 1",
        "project_id": "1c2f73e5-997c-4c93-aa81-c86fe15fa294",
        "selling_unit_id": "1c2f73e5-997c-4c93-aa81-c86fe15fa294",
        "public_price": 29.9,
        "description": "Un premier service",
        "created_at": "2023-04-14T05:34:47.782Z",
        "updated_at": "2023-04-14T05:34:47.782Z",
        "url": "string"
      };
      service.show("1c2f73e5-997c-4c93-aa81-c86fe15fa294", myService.id).subscribe(
        function(service: IService) {
          expect(service).toEqual(myService)
        }
      );

      const req = httpTestingController.expectOne(comService.build_url(`projects/1c2f73e5-997c-4c93-aa81-c86fe15fa294/services/${myService.id}`));
      req.flush(myService);
    }));
  });

  describe('#create', () => {
    it('should post new service information to server', fakeAsync(() => {
      let n_service = {
            "name": "Service 1",
            "selling_unit_id": "1c2f73e5-997c-4c93-aa81-c86fe15fa294",
            "public_price": 29.9,
            "description": "Un premier service",
          },
          myService = {
            ...n_service,
            "id": "6041fead-7cc9-483a-8937-a860e35aa910",
            "project_id": "1c2f73e5-997c-4c93-aa81-c86fe15fa294",
            "url": "string"
          };
      service.create("1c2f73e5-997c-4c93-aa81-c86fe15fa294", n_service).subscribe(function(_service: IService) {});

      const req = httpTestingController.expectOne({
        url: comService.build_url('projects/1c2f73e5-997c-4c93-aa81-c86fe15fa294/services'),
        method: 'post'
      });
      expect(JSON.parse(req.request.body)).toEqual({'service': n_service })
      req.flush(myService);
    }));
  });

  describe('#delete', () => {
    it('should generate a delete request on the good service', fakeAsync(() => {
      service.delete("1c2f73e5-997c-4c93-aa81-c86fe15fa294", "6041fead-7cc9-483a-8937-a860e35aa910")
             .subscribe(function(data: null) {
              expect(data).toBe(null);
             });
  
      const req = httpTestingController.expectOne({
        url: comService.build_url('projects/1c2f73e5-997c-4c93-aa81-c86fe15fa294/services/6041fead-7cc9-483a-8937-a860e35aa910'),
        method: 'delete'
      });
      req.flush(null);
    }));
  });

  describe('#update', () => {
    it('should patch service with the update information to server', fakeAsync(() => {
      let to_update = {
            "description": "Un premier service."
          },
          myService = {
            "id": "6041fead-7cc9-483a-8937-a860e35aa910",
            "name": "Service 1",
            "project_id": "1c2f73e5-997c-4c93-aa81-c86fe15fa294",
            "selling_unit_id": "1c2f73e5-997c-4c93-aa81-c86fe15fa294",
            "public_price": 29.9,
            "description": "Un premier service",
            "created_at": "2023-04-14T05:34:47.782Z",
            "updated_at": "2023-04-14T05:34:47.782Z",
            "url": "string"
          };
      service.update(myService.project_id, myService.id, to_update)
             .subscribe(function(_service: IService) {});

      const req = httpTestingController.expectOne({
        url: comService.build_url(`projects/${myService.project_id}/services/${myService.id}`),
        method: 'patch'
      });
      expect(JSON.parse(req.request.body)).toEqual({ 'service': to_update })
      req.flush(myService);
    }));
  });
});
