import { TestBed, fakeAsync } from '@angular/core/testing';

import { ProjectsService } from './projects.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { CommunicateService } from '../communicate.service';
import { IProject } from '../models';

describe('ProjectsService', () => {
  let service: ProjectsService,
      httpTestingController: HttpTestingController,
      comService: CommunicateService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ]
    });
    service = TestBed.inject(ProjectsService);
    httpTestingController = TestBed.inject(HttpTestingController);
    comService = TestBed.inject(CommunicateService);
  });
  
  afterEach(() => {
      httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#list', () => {
    it('should get available units', fakeAsync(() => {
      let myProjects: Array<IProject> = [
        {
          "id": "6041fead-7cc9-483a-8937-a860e35aa910",
          "owner_id": "1c2f73e5-997c-4c93-aa81-c86fe15fa294",
          "name": "Project One",
          "description": "The first project",
          "url": "string",
          created_at: "2023-04-14T05:34:47.782Z",
          updated_at: "2023-04-14T05:34:47.782Z"
        },
        {
          "id": "a440e9e6-8cb4-4cf0-9182-d1bbfbd8ed47",
          "owner_id": "1c2f73e5-997c-4c93-aa81-c86fe15fa294",
          "name": "Project Two",
          "description": "The second project",
          "url": "string",
          created_at: "2023-04-14T05:34:47.782Z",
          updated_at: "2023-04-14T05:34:47.782Z"
        }
      ];
      service.list().subscribe(
        function(units: Array<IProject>) {
          expect(units.length).toEqual(2)
          expect(units[0].id).toEqual(myProjects[0].id)
        }
      );

      const req = httpTestingController.expectOne({
        url: comService.build_url('projects'), method: 'get'
      });
      req.flush(myProjects);
    }));
  });

  describe('#show', () => {
    it('should get a unit information', fakeAsync(() => {
      let myProject = {
        "id": "6041fead-7cc9-483a-8937-a860e35aa910",
        "owner_id": "1c2f73e5-997c-4c93-aa81-c86fe15fa294",
        "name": "The project One",
        "description": "The first project",
        "url": "string",
        created_at: "2023-04-14T05:34:47.782Z",
        updated_at: "2023-04-14T05:34:47.782Z"
      };
      service.show(myProject.id).subscribe(
        function(unit: IProject) {
          expect(unit).toEqual(myProject)
        }
      );

      const req = httpTestingController.expectOne(comService.build_url(`projects/${myProject.id}`));
      req.flush(myProject);
    }));
  });
});
