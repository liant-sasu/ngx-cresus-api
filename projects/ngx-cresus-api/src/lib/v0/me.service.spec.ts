import { TestBed, fakeAsync, waitForAsync } from '@angular/core/testing';

import { MeService } from './me.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { CommunicateService } from '../communicate.service';
import { IUser } from '../models';

describe('MeService', () => {
  let service: MeService,
      httpTestingController: HttpTestingController,
      comService: CommunicateService;
  const myId = '1c2f73e5-997c-4c93-aa81-c86fe15fa294',
        myEmail = 'contact@liant.dev';

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ]
    });
    service = TestBed.inject(MeService);
    httpTestingController = TestBed.inject(HttpTestingController);
    comService = TestBed.inject(CommunicateService);
  }));
  
  afterEach(() => {
      httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#show', () => {
    it('should get my information from server', fakeAsync(() => {
      service.show().subscribe(
        function(me: IUser) {
          expect(me.id).toEqual(myId)
          expect(me.email).toEqual(myEmail)
        }
      );

      const req = httpTestingController.expectOne(comService.build_url('me'));
      req.flush({
        id: myId,
        email: myEmail
      });
    }));
  });
});
