import { TestBed, fakeAsync, waitForAsync } from '@angular/core/testing';

import { UnitsService } from './units.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { CommunicateService } from '../communicate.service';
import { IUnit } from '../models';

describe('UnitsService', () => {
  let service: UnitsService,
      httpTestingController: HttpTestingController,
      comService: CommunicateService;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ]
    });
    service = TestBed.inject(UnitsService);
    httpTestingController = TestBed.inject(HttpTestingController);
    comService = TestBed.inject(CommunicateService);
  }));
  
  afterEach(() => {
      httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#list', () => {
    it('should get available units', fakeAsync(() => {
      let myUnits = [
        {
          "id": "6041fead-7cc9-483a-8937-a860e35aa910",
          "creator_id": "1c2f73e5-997c-4c93-aa81-c86fe15fa294",
          "name": "cm",
          "description": "centimètre",
          "url": "string",
          "created_at": "2023-04-14T05:34:47.782Z",
          "updated_at": "2023-04-14T05:34:47.782Z"
        },
        {
          "id": "a440e9e6-8cb4-4cf0-9182-d1bbfbd8ed47",
          "creator_id": "1c2f73e5-997c-4c93-aa81-c86fe15fa294",
          "name": "kg",
          "description": "Kilogramme",
          "url": "string",
          "created_at": "2023-04-14T05:34:47.782Z",
          "updated_at": "2023-04-14T05:34:47.782Z"
        }
      ];
      service.list().subscribe(
        function(units: Array<IUnit>) {
          expect(units.length).toEqual(2)
          expect(units[0].id).toEqual(myUnits[0].id)
        }
      );

      const req = httpTestingController.expectOne({
        url: comService.build_url('units'), method: 'get'
      });
      req.flush(myUnits);
    }));
  });

  describe('#show', () => {
    it('should get a unit information', fakeAsync(() => {
      let myUnit = {
        "id": "6041fead-7cc9-483a-8937-a860e35aa910",
        "creator_id": "1c2f73e5-997c-4c93-aa81-c86fe15fa294",
        "name": "cm",
        "description": "centimètre",
        "url": "string",
        "created_at": "2023-04-14T05:34:47.782Z",
        "updated_at": "2023-04-14T05:34:47.782Z"
      };
      service.show(myUnit.id).subscribe(
        function(unit: IUnit) {
          expect(unit).toEqual(myUnit)
        }
      );

      const req = httpTestingController.expectOne(comService.build_url(`units/${myUnit.id}`));
      req.flush(myUnit);
    }));
  });

  describe('#create', () => {
    it('should post my information from server', fakeAsync(() => {
      let n_unit = {
            "creator_id": "1c2f73e5-997c-4c93-aa81-c86fe15fa294",
            "name": "cm",
            "description": "centimètre"
          },
          myUnit = {
            ...n_unit,
            "id": "6041fead-7cc9-483a-8937-a860e35aa910",
            "url": "string",
            "created_at": "2023-04-14T05:34:47.782Z",
            "updated_at": "2023-04-14T05:34:47.782Z"
          };
      service.create(n_unit).subscribe(function(_unit: IUnit) {});

      const req = httpTestingController.expectOne({
        url: comService.build_url('units'),
        method: 'post'
      });
      expect(JSON.parse(req.request.body)).toEqual({'unit': n_unit })
      req.flush(myUnit);
    }));
  });

  describe('#delete', () => {
    it('should generate a delete request on the good unit', fakeAsync(() => {
      service.delete("6041fead-7cc9-483a-8937-a860e35aa910")
             .subscribe(function(data: null) {
              expect(data).toBe(null);
             });
  
      const req = httpTestingController.expectOne({
        url: comService.build_url('units/6041fead-7cc9-483a-8937-a860e35aa910'),
        method: 'delete'
      });
      req.flush(null);
    }));
  });

  describe('#update', () => {
    it('should patch my information from server', fakeAsync(() => {
      let to_update = {
            "description": "centi-mètre"
          },
          myUnit = {
            "id": "6041fead-7cc9-483a-8937-a860e35aa910",
            "creator_id": "1c2f73e5-997c-4c93-aa81-c86fe15fa294",
            "name": "cm",
            "description": "centimètre",
            "url": "string"
          };
      service.update(myUnit.id, to_update)
             .subscribe(function(_unit: IUnit) {});

      const req = httpTestingController.expectOne({
        url: comService.build_url(`units/${myUnit.id}`),
        method: 'patch'
      });
      expect(JSON.parse(req.request.body)).toEqual({ 'unit': to_update })
      req.flush(myUnit);
    }));
  });
});
