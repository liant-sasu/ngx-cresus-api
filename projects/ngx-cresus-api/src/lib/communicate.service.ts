import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { IModels } from './models';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class CommunicateService {
  private _base_url: string = 'https://cresus-api.liant.dev'
  private api_version: string = 'v0'

  constructor(private http: HttpClient) { }

  build_url(path: string, api_version: string | null = this.api_version): string {
    var base_url = this.base_url;

    if(api_version !== null) {
       base_url += '/' + api_version;
    }

    return `${base_url}/${path}`;
  }

  
  public set base_url(v : string) {
    this._base_url = v;
  }

  
  public get base_url() : string {
    return this._base_url;
  }
  
  

  private headers() {
    return {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  }

  get<T=IModels>(path: string, params?: HttpParams): Observable<T> {
    return this.http.get<T>(
      this.build_url(path, path == 'version' ? null : this.api_version),
      { headers: this.headers(), params: params }
    );
  }

  post<T>(path: string, payload:any, params?: HttpParams): Observable<T> {
    return this.http.post<T>(
      this.build_url(path),
      JSON.stringify(payload),
      { headers: this.headers(), params: params }
    );
  }

  patch<T>(path: string, payload:any, params?: HttpParams): Observable<T> {
    return this.http.patch<T>(
      this.build_url(path),
      JSON.stringify(payload),
      { headers: this.headers(), params: params }
    );
  }

  delete(path: string, params?: HttpParams): Observable<null> {
    return this.http.delete<null>(
      this.build_url(path),
      { headers: this.headers(), params: params }
    );
  }
}
