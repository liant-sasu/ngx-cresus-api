import { NgModule } from '@angular/core';
import { NgxCresusApiComponent } from './ngx-cresus-api.component';



@NgModule({
  declarations: [
    NgxCresusApiComponent
  ],
  imports: [
  ],
  exports: [
    NgxCresusApiComponent
  ]
})
export class NgxCresusApiModule { }
