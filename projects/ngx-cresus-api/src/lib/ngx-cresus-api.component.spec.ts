import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NgxCresusApiComponent } from './ngx-cresus-api.component';

describe('NgxCresusApiComponent', () => {
  let component: NgxCresusApiComponent;
  let fixture: ComponentFixture<NgxCresusApiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NgxCresusApiComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NgxCresusApiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
