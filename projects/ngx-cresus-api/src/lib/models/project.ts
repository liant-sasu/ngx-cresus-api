import { ITimeable } from "./timeable";

export interface IProject extends ITimeable {
  id: string; // UUID
  name: string; // The name of the project
  owner_id: string; // User's uuid, can't be null
  description: string;
  url: string; // The URL to access the object at.
}
