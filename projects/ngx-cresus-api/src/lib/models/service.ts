import { ITimeable } from "./timeable";

export interface IServiceUpdate {
  name?: string;
  selling_unit_id?: string; // unit's UUID, can't be null
  public_price?: Number;
  description?: string;
}

export interface IServiceCreate {
  name: string;
  selling_unit_id: string; // budget's UUID, can't be null
  public_price: Number;
  description: string;
}

export interface IService extends ITimeable {
  id: string; // UUID
  name: string;
  project_id: string; // project's UUID, can't be null
  selling_unit_id: string; // budget's UUID, can't be null
  public_price: Number;
  description: string;
  url: string;
}
