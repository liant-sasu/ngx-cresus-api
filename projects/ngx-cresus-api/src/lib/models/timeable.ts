export interface ITimeable {
  created_at: string; // It's a string of date and time
  updated_at: string; // It's a string of date and time
}