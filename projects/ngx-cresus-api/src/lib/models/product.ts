import { ITimeable } from "./timeable";

export interface IProductUpdate {
  name?: string;
  selling_unit_id?: string; // unit's UUID, can't be null
  public_price?: Number;
  description?: string;
}

export interface IProductCreate {
  name: string;
  selling_unit_id: string; // budget's UUID, can't be null
  public_price: Number;
  description: string;
}

export interface IProduct extends ITimeable {
  id: string; // UUID
  name: string;
  project_id: string; // project's UUID, can't be null
  selling_unit_id: string; // budget's UUID, can't be null
  public_price: Number;
  description: string;
  url: string;
}
