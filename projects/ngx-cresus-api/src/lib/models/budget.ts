import { ITimeable } from "./timeable";

export interface IBudgetUpdate {
  name?: string;
  description?: string;
}

export interface IBudgetCreate {
  name: string;
  description: string;
}

export interface IBudget extends ITimeable {
  id: string; // UUID
  name: string;
  description: string;
  project_id: string; // project's UUID, can't be null
  url: string; // The url to access this resource
}
