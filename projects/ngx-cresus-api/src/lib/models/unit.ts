import { ITimeable } from "./timeable";

export interface IUnitUpdate {
  name?: string;
  creator_id?: string | null; // can become public, but can't be appropriated
  description?: string;
}

export interface IUnitCreate {
  name: string;
  creator_id: string | null; // when created as public, it will become 
  description: string;
}

export interface IUnit extends ITimeable {
  id: string; // UUID
  name: string;
  creator_id: string | null; // user's UUID if owned by someone, else this is a public non-editable unit
  description: string; // text field of description
  url: string; // The URL to access the object at.
}
