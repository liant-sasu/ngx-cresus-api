import { ITimeable } from "./timeable";

export enum EFrequencies {
  'daily', 'weekly', 'biweekly', 'monthly',
  'bimonthly', 'quarterly', 'semiannually',
  'annually'
};

export interface IChargeUpdate {
  name?: string;
  budget_id?: string | null; // budget's UUID, it can be null
  frequency?: EFrequencies;
  amount?: Number;
  description?: string;
}

export interface IChargeCreate {
  name: string;
  budget_id: string | null; // budget's UUID, it can be null
  frequency: EFrequencies;
  amount: Number;
  description: string;
}

export interface ICharge extends ITimeable {
  id: string; // UUID
  name: string;
  project_id: string; // project's UUID, can't be null
  budget_id: string | null; // budget's UUID, it can be null
  frequency: EFrequencies;
  amount: Number;
  description: string;
  url: string;
}
