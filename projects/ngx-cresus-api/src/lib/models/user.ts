import { ITimeable } from "./timeable";

export interface IUser extends ITimeable {
  id: string; // UUID
  email: string;
}