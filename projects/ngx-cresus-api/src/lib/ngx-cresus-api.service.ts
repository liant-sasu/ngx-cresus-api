import { Injectable } from '@angular/core';

import { environment } from '../environments/environment';
import { IBudgetCreate, IBudgetUpdate, IChargeCreate, IChargeUpdate, IProductCreate, IProductUpdate, IServiceCreate, IServiceUpdate, IUnitCreate, IUser, IVersion } from './models';
import { Observable } from 'rxjs/internal/Observable';
import { CommunicateService } from './communicate.service';
import { MeService } from './v0/me.service';
import { BudgetsService } from './v0/budgets.service';
import { ChargesService } from './v0/charges.service';
import { ProjectsService } from './v0/projects.service';
import { UnitsService } from './v0/units.service';
import { ProductsService } from './v0/products.service';
import { ServicesService } from './v0/services.service';

@Injectable({
  providedIn: 'root'
})
export class NgxCresusApiService {

  constructor(
    private comService: CommunicateService, private budgetsService: BudgetsService,
    private chargesService: ChargesService, private meService: MeService,
    private projectsService: ProjectsService, private unitsService: UnitsService,
    private productsService: ProductsService, private servicesService: ServicesService
  ) { }

  update_api_url(url: string) { this.comService.base_url = url }

  libVersion() {
    return environment.libVersion;
  }

  version(): Observable<IVersion> {
    return this.comService.get<IVersion>('version');
  }

  me(): Observable<IUser> {
    return this.meService.show();
  }

  /* Budget management */

  budgets(project_id: string) {
    return this.budgetsService.list(project_id);
  }

  budget(project_id: string, id: string) {
    return this.budgetsService.show(project_id, id);
  }

  create_budget(project_id: string, budget: IBudgetCreate) {
    return this.budgetsService.create(project_id, budget);
  }

  update_budget(project_id: string, id: string, update_data: IBudgetUpdate) {
    return this.budgetsService.update(project_id, id, update_data);
  }

  delete_budget(project_id: string, id: string) {
    return this.budgetsService.delete(project_id, id);
  }

  /* Charges management */

  charges(project_id: string) {
    return this.chargesService.list(project_id);
  }

  charge(project_id: string, id: string) {
    return this.chargesService.show(project_id, id);
  }

  create_charge(project_id: string, charge: IChargeCreate) {
    return this.chargesService.create(project_id, charge);
  }

  update_charge(project_id: string, id: string, update_data: IChargeUpdate) {
    return this.chargesService.update(project_id, id, update_data);
  }

  delete_charge(project_id: string, id: string) {
    return this.chargesService.delete(project_id, id);
  }

  /* Projects management */

  projects() {
    return this.projectsService.list();
  }

  project(id: string) {
    return this.projectsService.show(id);
  }

  /* units management */

  units() {
    return this.unitsService.list();
  }

  unit(id: string) {
    return this.unitsService.show(id);
  }

  create_unit(unit: IUnitCreate) {
    return this.unitsService.create(unit);
  }

  update_unit(id: string, update_data: IChargeUpdate) {
    return this.unitsService.update(id, update_data);
  }

  delete_unit(id: string) {
    return this.unitsService.delete(id);
  }

  /* Products management */

  products(project_id: string) {
    return this.productsService.list(project_id);
  }

  product(project_id: string, id: string) {
    return this.productsService.show(project_id, id);
  }

  create_product(project_id: string, product: IProductCreate) {
    return this.productsService.create(project_id, product);
  }

  update_product(project_id: string, id: string, update_data: IProductUpdate) {
    return this.productsService.update(project_id, id, update_data);
  }

  delete_product(project_id: string, id: string) {
    return this.productsService.delete(project_id, id);
  }

  /* Services management */

  services(project_id: string) {
    return this.servicesService.list(project_id);
  }

  service(project_id: string, id: string) {
    return this.servicesService.show(project_id, id);
  }

  create_service(project_id: string, service: IServiceCreate) {
    return this.servicesService.create(project_id, service);
  }

  update_service(project_id: string, id: string, update_data: IServiceUpdate) {
    return this.servicesService.update(project_id, id, update_data);
  }

  delete_service(project_id: string, id: string) {
    return this.servicesService.delete(project_id, id);
  }
}
